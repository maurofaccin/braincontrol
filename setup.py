#!/usr/bin/env python3
# Author: Mauro Faccin 2014
# -------------------------
# |   This is braincontrol|
# -------------------------
# |    License: GPL3      |
# |   see LICENSE.txt     |
# -------------------------

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
import braincontrol

setup(name='braincontrol',
      version=braincontrol.__version__,
      description=braincontrol.__description__,
      long_description=braincontrol.__long_description__,
      author=braincontrol.__author__,
      author_email=braincontrol.__author_email__,
      url=braincontrol.__url__,
      license=braincontrol.__copyright__,
      packages=['braincontrol'],
      requires=[
          'numpy',
          'scipy',
          'skimage',
          'statmodels',
          'jpype',
          'igraph',
          'subprocess (>=2.7)'],
      classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Physics'
        ],
      )
