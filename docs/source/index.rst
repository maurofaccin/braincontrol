.. braincontrol documentation master file, created by
   sphinx-quickstart2 on Mon Jun 15 15:25:19 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to braincontrol's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   control.rst 
   causality.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

