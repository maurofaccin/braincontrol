#!/usr/bin/env python3
# encoding: utf-8
"""
Get a np.ndarray().tobytes() of the tensor as argument,
compute the control, give it back as a
np.ndarray().tobytes()
"""

import sys

import numpy as np
import igraph as ig


def flow(tensor, weighted=False, weight_only=False):
    Nel, _, Ntimes = tensor.shape
    N = Ntimes * Nel

    # get edgelist and some parameter to build the graph
    gr = build_edgelist(tensor, weighted=weighted)
    flow = np.zeros(len(gr['innodes']), dtype=np.float)

    if weighted:
        nodeflow = np.zeros([Nel, Nel], dtype=np.float)

    for inn, iel in enumerate(gr['innodes']):
        G = ig.Graph(gr['N'], directed=True)
        G.add_edges(gr['edgelist'] + iel)
        # G.es['capacity'] = gr['weight']
        _flow = G.maxflow(gr['innode'], gr['outnode'], capacity=gr['weight'])
        flow[inn] = _flow.value
        if weighted:
            for (i, j), f in zip(gr['edgelist']+iel, _flow.flow):
                if i < N and j < N:
                    # we need to check either inward or outward edges
                    ni = i % Nel
                    nodeflow[inn, ni] += f

    if weighted:
        if weight_only:
            return nodeflow
        else:
            return np.array(flow), nodeflow
    else:
        return np.array(flow)


def build_edgelist(tensor, weighted=False):
    Nel, _, Ntimes = tensor.shape
    N = Ntimes * Nel

    out = {'edgelist': [],
           'innodes': [],
           'innode': None,
           'outnode': None,
           'N': None,
           'weight': None,
           }

    if weighted:
        out['weight'] = []

        def link(ti, i, tj, j):
            out_i = ti*Nel + i
            in_j = tj*Nel + j
            return (out_i, in_j)

        # add edges accordingly
        for (i, j, t), val in np.ndenumerate(tensor):
            if val > 0 and t < Ntimes-1:
                out['edgelist'].append(link(t, i, t+1, j))
                out['weight'].append(val)

        # outlinks to sink N+1
        for n in range(Nel):
            out['edgelist'].append(link(Ntimes-1, n, Ntimes, 1))
            out['weight'].append(1.0)

        # inlinks from source N
        for i in range(Nel):
            innode_el = []
            for t in range(Ntimes):
                innode_el.append(link(Ntimes, 0, t, i))

            out['innodes'].append(innode_el)
        out['weight'].extend([1.0]*Ntimes)

        out['N'] = N+2
        out['innode'] = N
        out['outnode'] = N+1
    else:
        def link(ti, i, tj=None, j=None):
            if tj is None and j is None:
                out_i = ti*Nel + i + N
                in_i = ti*Nel + i
                return (in_i, out_i)
            else:
                out_i = ti*Nel + i + N
                in_j = tj*Nel + j
                return (out_i, in_j)

        # split all nodes in in-nodes and out-nodes
        for t in range(Ntimes):
            for index in range(Nel):
                out['edgelist'].append(link(t, index))

        # add edges accordingly
        for (i, j, t), val in np.ndenumerate(tensor):
            if val > 0 and t < Ntimes-1:
                out['edgelist'].append(link(t, i, t+1, j))

        # outlinks to sink 2N+1
        for n in range(Nel):
            out['edgelist'].append(link(Ntimes-1, n, Ntimes, N+1))

        # inlinks from source 2N
        for i in range(Nel):
            innode_el = []
            for t in range(Ntimes):
                innode_el.append(link(Ntimes, 0, t, i))
            out['innodes'].append(innode_el)

        out['weight'] = [1.0] * (len(out['edgelist']) + Ntimes)
        out['N'] = 2*N+2
        out['innode'], out['outnode'] = 2*N, 2*N+1
    return out


if __name__ == '__main__':
    weight = True if '--weighted' in sys.argv else False
    with sys.stdin:
        stdin = sys.stdin.buffer.read()
    tensor = np.loads(stdin)

    control = flow(tensor, weighted=weight, weight_only=weight)

    contr_dumps = control.dumps()
    with sys.stdout:
        sys.stdout.buffer.write(contr_dumps)
