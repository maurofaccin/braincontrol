#!/usr/bin/env python3
# encoding: utf-8

# standard modules
import os
import json
import jpype
import itertools as it

# math modules
import numpy as np
from statsmodels.tsa.stattools import grangercausalitytests

# ========== Constants =============

# Java jidt interface
package_directory = os.path.dirname(os.path.abspath(__file__))
jarLocation = os.path.abspath(package_directory +
                              '/../info-dynamics/infodynamics.jar')
jpype.startJVM(jpype.getDefaultJVMPath(), "-ea",
               "-Djava.class.path="+jarLocation)
teCalcKernel = jpype.JPackage("infodynamics.measures.continuous.kernel")\
    .TransferEntropyCalculatorKernel
teCalcKraskov = jpype.JPackage("infodynamics.measures.continuous.kraskov")\
    .TransferEntropyCalculatorKraskov


BANDS = [u'unfiltered',
         u'gamma1', u'gamma2', u'gamma3',
         u'alpha',
         u'delta',
         u'mu',
         u'beta',
         u'theta']
# transfer entropy
# kraskov
EMBEDDED_HISTORY = 2
EMBEDDED_DELAY = 1
SOURCE_TARGET_DELAY = 0
KRASKOV_NEIGS = 4
# kernel
KERNEL_WIDTH = 0.3
SIGNIFICANCE_REALIZATIONS = 1000

# Granger causality
USE_GRANGER_BEST = True


class Experiment():
    def __init__(self, path=None, **kwargs):
        self.local_te = None
        self.te_params = {}
        if path is not None:
            self.load(path, **kwargs)

    def load(self, path,
             band='unfiltered',
             timerange=None,
             freq=500,
             delay=50,
             electrodes='all'):
        '''
        Load time series from file.

        Args:
            path (str): path to file

        KWargs:
            band (str): frequency band to use (default to "unfiltered")
            timerange (int or iterable): time range to use for transfer
                entropy::
                    type int - [0:timerange]
                    type iterable - [timerange[0]:timerange[1]]
                    type None - whole time series
            freq (int): frequency of time-series acquisition in Hz (defaults
                to 50).
            delay (int or float): expected maximum delay for information flow
                between brain regions in ms (defaults to 10).
            electrodes (list of str): list of electrodes to use, if `all` use
                them all (defaults to `all`)
        '''

        raw = np.load(path)

        self.name = path.split('/')[-1].split('.')[0]
        if band in raw.files:
            self.band = band
        elif band == 'unfiltered' and 'tseries' in raw.files:
            self.band = 'tseries'
        elif band == 'tseries' and 'unfiltered' in raw.files:
            self.band = 'unfiltered'
        else:
            raise Exception('Band {} not in data'.format(band))

        if electrodes == 'all':
            self.electrodes = raw['electrodes']
            erange = np.ones_like(raw['electrodes'], dtype=bool)
        else:
            erange = np.zeros_like(raw['electrodes'], dtype=bool)
            for el in electrodes:
                if el not in raw['electrodes']:
                    raise Exception('Electrod {} not in data'.format(el))
                else:
                    indx = np.argwhere(raw['electrodes'] == el)[[0]]
                    erange[indx] = True
            self.electrodes = raw['electrodes'][erange]

        raw_Nel, raw_Nt = raw[self.band].shape
        if timerange is None:
            trange = range(0, raw_Nt)
        elif type(timerange) == int:
            trange = range(0, timerange)
        else:
            trange = range(timerange[0], timerange[1])
        self.tseries = raw[self.band][erange, :][:, trange]

        _, self.length = self.tseries.shape
        self.freq = freq
        self.delay = delay

    def __len__(self):
        return self.length

    def number_of_electrodes(self):
        return len(self.electrodes)

    def transfer_entropy(self,
                         max_delay=None,
                         **kwargs):

        if max_delay is None:
            max_d = int(self.delay * self.freq * 1e-3 + 0.5)
        else:
            max_d = max_delay
        self.te_params = {'max_delay': max_d}
        self.te_params.update(kwargs)

        if self.local_te is None:
            te = np.zeros([self.number_of_electrodes(),
                           self.number_of_electrodes(),
                           self.length])

            for iel1, iel2 in it.combinations(
                    range(self.number_of_electrodes()), 2):

                # time series of elects 1 and 2
                v1 = self.tseries[iel1, :]
                v2 = self.tseries[iel2, :]

                # transfer entropy of elects 1 and 2
                te12 = local_TE_delay(v1, v2, max_delay=max_d, **kwargs)
                te21 = local_TE_delay(v2, v1, max_delay=max_d, **kwargs)

                # store it
                te[iel1, iel2, :] = te12
                te[iel2, iel1, :] = te21

            # store in the class
            self.local_te = te

    def get_parameters(self):
        params = {}
        params['time_length'] = self.length
        params['electrodes'] = self.electrodes.tolist()
        params['name'] = self.name
        params['band'] = self.band
        params['freq'] = self.freq
        params['delay'] = self.delay
        params.update(self.te_params)
        if 'neig' not in params:
            params['neig'] = KRASKOV_NEIGS
        if 'hist' not in params:
            params['hist'] = EMBEDDED_HISTORY
        if 'embedding_delay' not in params:
            params['embedding_delay'] = EMBEDDED_DELAY
        if 'delay' not in params:
            params['delay'] = SOURCE_TARGET_DELAY

        return params

    def save_te(self, folder_path):
        fname = folder_path.rstrip('/') + '/'
        fname += self.name + '-' + self.band
        np.savez_compressed(fname, causality=self.local_te)
        with open(fname+'.json', 'w') as fout:
            json.dump(self.get_parameters(), fout, indent=4)


def local_TE(v1, v2, neig=KRASKOV_NEIGS,
             hist=EMBEDDED_HISTORY,
             embedding_delay=EMBEDDED_DELAY,
             delay=SOURCE_TARGET_DELAY,
             normalize='true'):
    ''' Compute the local transfer entropy

    .. math::
        T_{Y -> X}(k,l,u) = H(X_{n+1} | X_n^k) - H(X_{n+1} | X_n^k, Y_{n+1-u}^l)

    :v1: Source of information (Y)
    :v2: Target of information flow (X)
    :neig: kraskov neighbor number
    :hist: dimensionality of the embedding (l=k)
    :delay: source/target delay (u)

    :return: local transfer entropy (np.array)
    '''
    teKraskov = teCalcKraskov()
    teKraskov.initialise(hist, embedding_delay,
                         hist, embedding_delay,
                         delay)
    teKraskov.setProperty("NORMALISE", normalize)
    # Use Kraskov parameter K=4 for 4 nearest points
    teKraskov.setProperty("k", str(neig))
    if type(v1) == list:
        jv1 = jpype.JArray(jpype.JDouble, 1)(v1)
    else:
        jv1 = jpype.JArray(jpype.JDouble, 1)(v1.tolist())
    if type(v2) == list:
        jv2 = jpype.JArray(jpype.JDouble, 1)(v2)
    else:
        jv2 = jpype.JArray(jpype.JDouble, 1)(v2.tolist())
    teKraskov.setObservations(jv1, jv2)
    array = teKraskov.computeLocalOfPreviousObservations()
    return np.array(array).squeeze()


def local_TE_delay(v1, v2, max_delay=30, func='max', **kwargs):
    if max_delay == 0 or max_delay is None:
        return local_TE(v1, v2, **kwargs)
    else:
        te = np.zeros([len(v1), max_delay])
        for dl in range(max_delay):
            te[:, dl] = local_TE(v1, v2, delay=dl, **kwargs)
        if func == 'max':
            return te.max(1).squeeze()
        elif func == 'mean':
            return te.mean(1).squeeze()
        else:
            raise NotImplementedError('use max or mean please!')


def transfer_entropy(source, target=None, estimator='kraskov'):
    if target is None:
        s = source[0]
        t = source[1]
    else:
        s = source
        t = target

    if type(s) == np.ndarray:
        s = s.tolist()
    if type(t) == np.ndarray:
        t = t.tolist()

    if estimator == 'kraskov':
        # Normalise the individual variables
        teCalc = teCalcKraskov()
        teCalc.setProperty('NORMALIZE', 'true')
        # Use Kraskov parameter K=4 for 4 nearest points
        teCalc.setProperty('k', str(KRASKOV_NEIGS))
        # Use history length 1 (Schreiber k=1),
        # kernel width of 0.3 normalised units
        teCalc.initialise(EMBEDDED_HISTORY)
    elif estimator == 'kernel':
        teCalc = teCalcKernel()
        # Normalise the individual variables
        teCalc.setProperty('NORMALIZE', 'true')
        # Use history length 1 (Schreiber k=1),
        # kernel width of 0.3 normalised units
        teCalc.initialise(EMBEDDED_HISTORY, KERNEL_WIDTH)
    else:
        print('unknown estimator')
        exit()
    teCalc.setObservations(jpype.JArray(jpype.JDouble, 1)(s),
                           jpype.JArray(jpype.JDouble, 1)(t))
    rte = teCalc.computeSignificance(SIGNIFICANCE_REALIZATIONS)
    results = dict(te=rte.actualValue, pvalue=rte.pValue)
    # te = teCalc.computeAverageLocalOfObservations()
    # results = dict(te = te, pvalue = 0)
    return results


def granger_causality(source, target=None, order=5, test='ssr_ftest'):
    ''' compute granger between two series (lists)

    max allowed order = len(source|targer) / 3 - 1
    '''
    if target is None:
        s = source[0]
        t = source[1]
    else:
        s = source
        t = target

    granger = dict(granger=0, pvalue=10.0, order=0)
    # the second causes the first
    tslice = np.asarray([t, s]).T

    g = grangercausalitytests(tslice, order, verbose=False)

    if USE_GRANGER_BEST:
        for o in range(1, order+1):
            if granger['pvalue'] > g[o][0][test][1]:
                granger['granger'] = g[o][0][test][0]
                granger['pvalue'] = g[o][0][test][1]
                granger['order'] = o
    else:
        granger['granger'] = g[order][0][test][0]
        granger['pvalue'] = g[order][0][test][1]
        granger['order'] = order

    return granger
