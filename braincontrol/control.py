#!/usr/bin/env python3
# encoding: utf-8

# standard modules
import os
import subprocess
import gzip
import json
import itertools as it

# math modules
import numpy as np
from skimage.filters import threshold_otsu

# ========== Constants =============

BANDS = [u'unfiltered',
         u'gamma1', u'gamma2', u'gamma3',
         u'alpha',
         u'delta',
         u'mu',
         u'beta',
         u'theta']
package_directory = os.path.dirname(os.path.abspath(__file__))


class TNet():
    '''simpler temporal network class
    '''

    def __init__(self, filename=None, **kwargs):
        self.edges = []
        self.nodes = []
        self.times = []
        if filename is not None:
            self.read_edges(filename, **kwargs)

    def add_edge(self, t, source, target, weight=1.0, pvalue=0.0):
        self.edges.append(dict(time=t,
                               source=source,
                               target=target,
                               weight=weight))

        self.add_node(source)
        self.add_node(target)
        self.add_time(t)

    def add_node(self, node):
        if node not in self.nodes:
            self.nodes.append(node)

    def add_time(self, time):
        if time not in self.times:
            self.times.append(time)

    def read_tensor(self, filename, diff=True, t_range=None,
                    weight_cutoff=-np.infty):

        te_tensor = np.load(filename)
        with open(filename[:-3] + 'json', 'r') as fin:
            params = json.load(fin)

        Nel, _, Ntimes = te_tensor.shape

        for t in range(Ntimes):

            for i, j in it.combinations(range(Nel), 2):
                eli = params['electrodes'][i]
                elj = params['electrodes'][j]
                if diff:
                    te_local = te_tensor[i, j, t] - te_tensor[j, i, t]
                    if te_local > weight_cutoff:
                        self.add_edge(t, eli, elj, weight=te_local)
                else:
                    if te_tensor[i, j, t] > weight_cutoff:
                        self.add_edge(t, eli, elj, weight=te_tensor[i, j, t])
                    if te_tensor[j, i, t] > weight_cutoff:
                        self.add_edge(t, elj, eli, weight=te_tensor[j, i, t])

    def read_edges(self, filename, diff=True, t_range=None,
                   weight_cutoff=None, pvalue_cutoff=1.0):
        '''read a file of the form
        t source target TE_ij TE_ji
        '''

        with gzip.open(filename, 'r') as fin:
            for line in fin:
                if line.strip()[0] == '#':
                    # this is a comment
                    pass
                else:
                    l = line.split()
                    t = int(l[0])
                    if t_range is not None:
                        if t < t_range[0] or t > t_range[1]:
                            t = None

                    if t is not None:
                        source, target = l[1:3]
                        if diff:
                            # in this case we cannot compute a pvalue for
                            # the resulting edge
                            if len(l) > 5:
                                te = float(l[3])-float(l[5])
                            else:
                                te = float(l[3])-float(l[4])

                            if te > weight_cutoff:
                                self.add_edge(t, source, target, te)
                            elif te < -weight_cutoff:
                                self.add_edge(t, target, source, -te)
                        else:
                            if len(l) > 5:
                                te_ij, te_ji = float(l[3]), float(l[5])
                                pv_ij, pv_ji = float(l[4]), float(l[6])
                            else:
                                te_ij, te_ji = float(l[3]), float(l[4])
                                pv_ij, pv_ji = 0.0, 0.0

                            if te_ij > weight_cutoff and\
                               pv_ij <= pvalue_cutoff:
                                self.add_edge(t, source, target, te_ij, pv_ij)
                            if te_ji > weight_cutoff and\
                               pv_ji <= pvalue_cutoff:
                                self.add_edge(t, target, source, te_ji, pv_ji)

    def number_of_edges(self):
        return len(self.edges)

    def node_to_index(self, time, node, t0=0):
        nodeindex = self.nodes.index(node)
        return (time-t0)*len(self.nodes) + nodeindex


def tensor_null(tensor, mode='tshuffle',
                kave=None):
    Nel, _, Nt = tensor.shape
    if mode == 'tshuffle':
        shuffled = tensor[:, :, np.random.permutation(Nt)]
    elif mode == 'configmodel':
        shuffled = tensor.copy()
        shuffled[shuffled > 0] = 1
        for t in range(Nt):
            shuffled[:, :, t] = configuration_model(shuffled[:, :, t])
    elif mode == 'randommodel':
        if kave is None:
            # the following consider only positive valued tensors
            k = np.count_nonzero(tensor) / (Nt * Nel)
        else:
            kave = k

        shuffled = np.zeros_like(tensor)
        diag = np.arange(Nel)*Nel + np.arange(Nel)
        x = np.setdiff1d(range(Nel**2), diag)
        for t in range(Nt):
            a = np.zeros(Nel**2)
            xsample = np.random.permutation(x)[:k*Nel]
            a[xsample] = 1
            shuffled[:, :, t] = a.reshape([Nel, Nel])
    return shuffled


def tensor_filter(tensor, intime=0, outtime=None,
                  co_type='absolute', diff=True,
                  local=False,
                  kave=None,
                  return_cutoff=False,
                  cutoff=0.0):
    Nel, _, Ntime = tensor.shape

    if outtime is None:
        _ot = Ntime
    else:
        _ot = outtime

    if diff:
        _tsr = tensor[:, :, intime:_ot]
        _tsr = _tsr - np.transpose(_tsr, axes=[1, 0, 2])
    else:
        _tsr = tensor[:, :, intime:_ot]

    _tsr[_tsr < 0] = 0.0
    _, _, ntimes = _tsr.shape
    if co_type == 'absolute':
        threshold = cutoff
    elif co_type == 'relative':
        if local:
            threshold = []
            for t in range(ntimes):
                tsr_lin = _tsr[:, :, t]
                tsr_lin = tsr_lin[tsr_lin > 0]
                threshold.append(np.percentile(tsr_lin, (1.0-cutoff)*100))
        else:
            tsr_lin = _tsr[_tsr > 0]
            threshold = np.percentile(tsr_lin, (1.0-cutoff)*100)
    elif co_type == 'backbone':
        # this is already a local filtering
        # see Vespignani et al.
        k = (_tsr > 0).sum(1).reshape([Nel, 1, ntimes])
        p = _tsr[:, :, :]
        # do not consider nodes without outgoing links
        p[k <= 0] = 1.0
        p = p / p.sum(1).reshape([Nel, 1, ntimes])
        # Alpha
        threshold = np.power(1.0-p, k-1.0)
        # this approach have different selection rule:
        _tsr[threshold > cutoff] = 0
        if return_cutoff:
            return _tsr, threshold
        else:
            return _tsr
    elif co_type == 'backbone-slow':
        # this is already a local filtering
        # see Vespignani et al.
        for el, t in it.product(range(Nel), range(ntimes)):
            p = _tsr[el, :, t]
            k = np.sum(p > 0)
            if k > 0:
                p = p/np.sum(p)
                alpha = np.power(1.0-p, k-1.0)
                _tsr[el, alpha > cutoff, t] = 0.0
        return _tsr
    elif co_type == 'otsu':
        # get threshold of only actual links (with positive weights)
        if local:
            threshold = []
            for t in range(ntimes):
                tsr_lin = _tsr[:, :, t]
                tsr_lin = tsr_lin[tsr_lin > 0]
                threshold.append(threshold_otsu(tsr_lin))
        else:
            threshold = threshold_otsu(_tsr[_tsr > 0])
    elif co_type == 'kave':
        # choose links based on the value of the wanted mean degree
        if local:
            # Links to remove for each time slice
            Nlink = Nel*Nel - int(Nel*kave)
            _tsr = _tsr.reshape([Nel*Nel, ntimes])
            indx = _tsr.argsort(0)
            for t in range(ntimes):
                _tsr[indx[:Nlink, t], t] = 0
        else:
            Nlink = (Nel*Nel - int(Nel*kave))*ntimes
            _tsr = _tsr.flatten()
            indx = _tsr.argsort(0)
            _tsr[indx[:Nlink]] = 0

        if return_cutoff:
            return _tsr.reshape([Nel, Nel, ntimes]), None
        else:
            return _tsr.reshape([Nel, Nel, ntimes])

    _tsr[_tsr < threshold] = 0.0
    if return_cutoff:
        return _tsr, threshold
    else:
        return _tsr


def flow(tensor, weight=False):
    tensor_dumps = tensor.dumps()
    command = ['python3', package_directory+'/flow.py']
    if weight:
        command.append('--weighted')
    proc = subprocess.Popen(command,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            bufsize=0,
                            )
    output, _ = proc.communicate(tensor_dumps)
    control = np.loads(output)
    return control


def pop_diag(mat):
    pop_mat = np.tril(mat)[:, :-1]
    pop_mat += np.triu(mat)[:, 1:]
    return pop_mat


def push_diag(mat, vals=0):
    M, N = mat.shape
    push_mat = np.zeros([M, M], dtype=int)
    push_mat += np.tril(np.append(mat, np.zeros([M, 1], dtype=int), axis=1), -1)
    push_mat += np.triu(np.append(np.zeros([M, 1], dtype=int), mat, axis=1), 1)
    np.fill_diagonal(push_mat, vals)
    return push_mat


def configuration_model(arr):
    '''Return an adjacency matrix with the same in and out degree
    distributions.
    '''
    in_arr = arr.copy()
    in_arr[in_arr > 0] = 1
    out = np.random.permutation(in_arr.sum(0))
    ind = np.random.permutation(in_arr.sum(1))
    dist = np.outer(ind, out.T)
    ddist = pop_diag(dist) + 1e-3
    dsample = np.random.choice(range(ddist.size),
                               size=in_arr.sum(),
                               replace=False,
                               p=ddist.flatten()/ddist.sum())
    shuffled = np.zeros(ddist.size, dtype=int)
    shuffled[dsample] = 1
    return push_diag(shuffled.reshape(ddist.shape), vals=0)
