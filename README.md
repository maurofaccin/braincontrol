# Brain Control

This is a python module for controllability on brain causality networks.

It is intended to provide functions and classes to reproduce data from 
the corresponding publication.